//
//  JPMCConstants.h
//  JPMCiOSExercise
//
//  Created by Bedre, Karthik on 11/18/16.
//  Copyright © 2016 iOS Exercise. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const kClassJPMCBranchLocatorViewController;
extern NSString *const kClassJPMCLocationDetailViewController;
