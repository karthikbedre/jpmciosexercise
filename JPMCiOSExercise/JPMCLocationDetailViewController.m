//
//  JPMCLocationDetailViewController.m
//  JPMCiOSExercise
//
//  Created by Bedre, Karthik on 11/19/16.
//  Copyright © 2016 iOS Exercise. All rights reserved.
//

#import "JPMCLocationDetailViewController.h"

static NSString *kNavigationTitle = @"Location Detail";

@interface JPMCLocationDetailViewController ()<UITableViewDelegate, UITableViewDataSource>

@property(nonatomic, weak) IBOutlet UITableView *tableViewLocationDetail;

@end

@implementation JPMCLocationDetailViewController

- (void)viewDidLoad {
	
    [super viewDidLoad];
	self.navigationTitle = kNavigationTitle;
	[self filterDictonaryForAvailableValues];
}

- (void)didReceiveMemoryWarning {
	
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableViewDataSources and Deleagates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
	
	if([self isTableTypeDetail])
		return [(NSDictionary *)self.locationDetail allKeys].count;
	else
		return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	
	if([self isTableTypeDetail]){
		if([[(NSDictionary *)self.locationDetail objectForKey:[self getSectionTitleForIndex:section]] isKindOfClass:[NSArray class]]){
			return [(NSArray *)[(NSDictionary *)self.locationDetail objectForKey:[self getSectionTitleForIndex:section]] count];
		}
		else
			return 1;
	}
	else
		return [(NSArray *)self.locationDetail count];
	
	return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	
	static NSString *cellIdentifier = @"defaultCell";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	if (cell == nil)
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
	
	cell.textLabel.text = [self getCellTitleForIndex:indexPath];
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	
	return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
	
	if([self getSectionTitleForIndex:section]){
		
		UIView *viewBackground=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 25.0)];
		
		NSString * title = [self getSectionTitleForIndex:section];
		
		UILabel *lblTitle = [[UILabel alloc]initWithFrame:viewBackground.frame];
		lblTitle.text = [title capitalizedString];
		lblTitle.backgroundColor = [UIColor grayColor];
		lblTitle.textColor=[UIColor whiteColor];
		lblTitle.font = [UIFont boldSystemFontOfSize:15];
		[viewBackground addSubview:lblTitle];
		return viewBackground;
	}
	return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
	return 25.0;
}

#pragma mark - Helper Methods.

- (NSString *)getSectionTitleForIndex:(NSInteger)index{
	
	if([self.locationDetail isKindOfClass:[NSDictionary class]] && [(NSDictionary *)self.locationDetail count] > 0){
		if([(NSDictionary *)self.locationDetail allKeys].count >= index){
			NSString *keyAtIndex = [(NSDictionary *)self.locationDetail allKeys][index];
			return keyAtIndex;
		}
	}
	return nil;
}

- (NSString *)getCellTitleForIndex:(NSIndexPath *)indexPath{
	
	if([self.locationDetail isKindOfClass:[NSDictionary class]] && [(NSDictionary *)self.locationDetail count] > 0){
		if([(NSDictionary *)self.locationDetail allKeys].count >= indexPath.section){
			NSString *keyAtIndex = [(NSDictionary *)self.locationDetail allKeys][indexPath.section];
			id value = [(NSDictionary *)self.locationDetail objectForKey:keyAtIndex];
			if([value isKindOfClass:[NSArray class]])
				return [NSString stringWithFormat:@"%@",[(NSArray *)value objectAtIndex:indexPath.row]];
			else
				return [NSString stringWithFormat:@"%@",value];
		}
	}
	
	return @"";
}
- (BOOL)isTableTypeDetail{
	
	if([self.locationDetail isKindOfClass:[NSDictionary class]]){
		return YES;
	}
	else
		return NO;
}

- (void)filterDictonaryForAvailableValues{
	NSDictionary *dictionary = self.locationDetail;
	
	if(![dictionary isKindOfClass:[NSDictionary class]])
		return ;
	
	NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
	NSArray *allKeys = [dictionary allKeys];
	
	for(NSString *key in allKeys){
		
		id value = dictionary[key];
		
		if([value isKindOfClass:[NSString class]]){
			
			NSString *str = dictionary[key];
			if(str.length == 0)
				continue;
		}
		else if([value isKindOfClass:[NSNumber class]]){
			
			NSNumber *num = value;
			if(num.intValue == 0)
				continue;
		}
		else if([value isKindOfClass:[NSArray class]]){
			
			NSArray *array = value;
			if(array.count == 0)
				continue;
		}
		
		[dict setObject:value forKey:key];
	}
	
	self.locationDetail = [NSDictionary dictionaryWithDictionary:dict];
}
@end
