//
//  JPMCErrorManager.m
//  JPMCiOSExercise
//
//  Created by Bedre, Karthik on 11/19/16.
//  Copyright © 2016 iOS Exercise. All rights reserved.
//

#import "JPMCErrorManager.h"
#import "JPMCUtility.h"

@implementation JPMCErrorManager

+ (void)showAlertForError:(NSError *)error{
	
	if(error.code == NSURLErrorNotConnectedToInternet || error.code == NSURLErrorNetworkConnectionLost){
		
		[JPMCUtility showAlertWithtitle:@"Internet Connectivity Lost" alertMessage:@"Please check you internet connection and try again." delegateModel:nil tag:0 cancelButtonTitle:@"OK" otherTitles:nil];
	}
	else{
		
		[JPMCUtility showAlertWithtitle:@"Service Maintainence" alertMessage:@"We are currently performing few maintainence to our servies due to which some functionalities may not be available, please chheck back soon." delegateModel:nil tag:0 cancelButtonTitle:@"OK" otherTitles:nil];
	}
}

@end
