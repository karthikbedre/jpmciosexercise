//
//  JPMCBaseDataModel.m
//  JPMCiOSExercise
//
//  Created by Bedre, Karthik on 11/18/16.
//  Copyright © 2016 iOS Exercise. All rights reserved.
//

#import "JPMCBaseDataModel.h"
#import <objc/runtime.h>

@implementation JPMCBaseDataModel

-(NSDictionary *)mappedDictionary
{
	unsigned int count = 0;
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	objc_property_t *properties = class_copyPropertyList([self class], &count);
	
	for (int i = 0; i < count; i++){
		
		@try {
			NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
			id value = [self valueForKey:key];
			
			if (value == nil){
				
				// nothing to do
			}
			else if ([value isKindOfClass:[NSString class]] || [value isKindOfClass:[NSNumber class]] || [value isKindOfClass:[NSDictionary class]] || [value isKindOfClass:[NSMutableDictionary class]] || [value isKindOfClass:[NSArray class]] || [value isKindOfClass:[NSMutableArray class]]){
				
				// TODO: extend to other types
				[dictionary setObject:value forKey:key];
			}
			else if ([value isKindOfClass:[NSObject class]]){
				
				[dictionary setObject:[value mappedDictionary] forKey:key];
			}
			else{
				
				NSLog(@"Invalid type for %@ (%@)", NSStringFromClass([self class]), key);
			}

		} @catch (NSException *exception) {
			
		} @finally {
			
		}
	}
	free(properties);
	return dictionary;
}

- (NSDictionary *)filterDictionaryForDesiredProperties:(NSDictionary *)dictionary
{
	u_int count;
	objc_property_t* properties = class_copyPropertyList([self class], &count);
	NSMutableArray* propertyArray = [NSMutableArray arrayWithCapacity:count];
	for (int i = 0; i < count ; i++)
	{
		const char* propertyName = property_getName(properties[i]);
		[propertyArray addObject:[NSString  stringWithCString:propertyName encoding:NSUTF8StringEncoding]];
	}
	free(properties);
	
	NSMutableDictionary *filteredDict = nil;
	if(propertyArray.count > 0)
		filteredDict = [[NSMutableDictionary alloc] init];
	for(NSString *name in propertyArray)
	{
		if(dictionary[name])
		{
			[filteredDict setObject:dictionary[name] forKey:name];
		}
	}
	
	return filteredDict;
}

@end
