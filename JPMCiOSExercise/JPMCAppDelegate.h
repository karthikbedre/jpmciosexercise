//
//  JPMCAppDelegate.h
//  JPMCiOSExercise
//
//  Created by Bedre, Karthik on 11/18/16.
//  Copyright © 2016 iOS Exercise. All rights reserved.
//

#import <UIKit/UIKit.h>

@import AFNetworking;

@interface JPMCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

+ (BOOL)isRechable;

@end

