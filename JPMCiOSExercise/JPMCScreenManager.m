//
//  JPMCScreenManager.m
//  JPMCiOSExercise
//
//  Created by Bedre, Karthik on 11/18/16.
//  Copyright © 2016 iOS Exercise. All rights reserved.
//

#import "JPMCScreenManager.h"

@implementation JPMCScreenManager

+ (JPMCScreenManager *)sharedInstance{
	
	static JPMCScreenManager *singletonInstance = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		singletonInstance = [[[self class] alloc] init];
	});
	
	return singletonInstance;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.view.backgroundColor = [UIColor whiteColor];
	self.toolbarHidden = YES;
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
	
	if([[self topViewController] isEqual:viewController]){
		
		[viewController viewWillAppear:YES];
		[viewController viewDidAppear:YES];
		return ;
	}
	[super pushViewController:viewController animated:animated];
}

- (UIViewController *)popViewControllerAnimated:(BOOL)animated{
	
	return [super popViewControllerAnimated:animated];
}

- (NSArray *)popToViewController:(UIViewController *)viewController animated:(BOOL)animated{
	
	if([[self topViewController] isEqual:viewController]){
		
		[viewController viewWillAppear:YES];
		[viewController viewDidAppear:YES];
		return nil;
	}
	return [super popToViewController:viewController animated:animated];
}

- (NSArray *)popToRootViewControllerAnimated:(BOOL)animated{
	
	return [super popToRootViewControllerAnimated:animated];
}

- (void)showViewWithName:(NSString *)vcName animated:(BOOL)animated{
	
	[[NSOperationQueue mainQueue] addOperationWithBlock:^{
		
		UIViewController *viewController = [self viewControllerInStackWithName:vcName];
		
		if (viewController){
			
			if([[self topViewController] isEqual:viewController]){
				
				[viewController viewWillAppear:YES];
				[viewController viewDidAppear:YES];
				return ;
			}
			[self popToViewController:viewController animated:animated];
		}
		else{
			
			viewController = [[NSClassFromString(vcName) alloc] init];
			[self pushViewController:viewController animated:animated];
		}
	}];
}

- (void)showViewController:(UIViewController *)controller animated:(BOOL)animated{
	
	[[NSOperationQueue mainQueue] addOperationWithBlock:^{
		[self pushViewController:controller animated:animated];
	}];
}

- (void)navigateToViewController:(UIViewController *)controller animated:(BOOL)animated{
	
	[[NSOperationQueue mainQueue] addOperationWithBlock:^{
		if ([self.viewControllers containsObject:controller]){
			
			[self popToViewController:controller animated:animated];
		}
		else{
			
			[self pushViewController:controller animated:animated];
		}
	}];
}

- (UIViewController *)viewControllerWithName:(NSString *)viewControllerName{
	
	return ([self viewControllerInStackWithName:viewControllerName] ?: [[NSClassFromString(viewControllerName) alloc] init]);
}

- (UIViewController *)viewControllerInStackWithName:(NSString *)viewControllerName{
	
	UIViewController *viewController = nil;
	
	for (UIViewController *controller in self.viewControllers){
		
		if ([NSStringFromClass([controller class]) isEqualToString:viewControllerName]){
			
			viewController = controller;
			break;
		}
	}
	
	return viewController;
}

- (void)didReceiveMemoryWarning {
	
    [super didReceiveMemoryWarning];
}

@end
