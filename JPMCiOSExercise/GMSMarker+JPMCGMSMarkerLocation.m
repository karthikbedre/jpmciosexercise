//
//  GMSMarker+JPMCGMSMarkerLocation.m
//  JPMCiOSExercise
//
//  Created by Bedre, Karthik on 11/19/16.
//  Copyright © 2016 iOS Exercise. All rights reserved.
//

#import "GMSMarker+JPMCGMSMarkerLocation.h"
#import <objc/runtime.h>
#import "JPMCBranchLocation.h"

static NSString *const kBranchLocation = @"branchLocation";

@implementation GMSMarker (JPMCGMSMarkerLocation)

- (void)setBranchLocation:(JPMCBranchLocation *)branchLocation{
	
	objc_setAssociatedObject(self, &kBranchLocation, branchLocation, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (JPMCBranchLocation *)branchLocation{
	
	id location = objc_getAssociatedObject(self, &kBranchLocation);
	if(!location || ![location isKindOfClass:[JPMCBranchLocation class]])
	{
		return nil;
	}
	
	return location;
}

@end
