//
//  JPMCScreenManager.h
//  JPMCiOSExercise
//
//  Created by Bedre, Karthik on 11/18/16.
//  Copyright © 2016 iOS Exercise. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JPMCScreenManager : UINavigationController

// Returns Singleton instance of the class.
+ (JPMCScreenManager *)sharedInstance;

/**
 *  Use this to navigate/navigate-to a viewController by its name.
 *
 *  @param vcName : Pass Servicekey to get the respective endpoint and configuration.
 *  @param animated : Pass YES/NO if animation is required on navigating.
 */
- (void)showViewWithName:(NSString*)vcName animated:(BOOL)animated;


/**
 Use this method to get a viewController's instance from navigation stack if available else it creates a new instance.

 @param viewControllerName : Pass the ViewControler's name as String.

 @return : Returns found/created viewController instance.
 */
- (UIViewController *)viewControllerWithName:(NSString *)viewControllerName;


/**
 Use this to push a UIViewController irrespective of that controllers instance already present in stack.

 @param controller : Pass the controller's instance to be pushed.
 @param animated : Pass YES/NO if animation is required on navigating.
 */
- (void)showViewController:(UIViewController *)controller animated:(BOOL)animated;


/**
 Use this to Push-to or Pop-to a UIViewControler.

 @param controller  : Pass the controller's instance to be pushed.
 @param animated   : Pass YES/NO if animation is required on navigating.
 */
- (void)navigateToViewController:(UIViewController *)controller animated:(BOOL)animated;

@end
