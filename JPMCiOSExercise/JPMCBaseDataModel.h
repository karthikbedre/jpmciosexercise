//
//  JPMCBaseDataModel.h
//  JPMCiOSExercise
//
//  Created by Bedre, Karthik on 11/18/16.
//  Copyright © 2016 iOS Exercise. All rights reserved.
//

/**
	Use this class to make use of object mapping of a JSON dictionary.
 */

#import <Foundation/Foundation.h>

@interface JPMCBaseDataModel : NSObject

@property(nonatomic, readonly) NSDictionary *mappedDictionary;

/**
 Use this method to filter a NSDictionary based on Property's available in JPMCBaseDataModel subclass.

 @param dictionary : Pass the Master dictionary to be filtered.

 @return : Returns a dictionary which can be used to map to JPMCBaseDataModel subclass.
 */
- (NSDictionary *)filterDictionaryForDesiredProperties:(NSDictionary *)dictionary;

@end
