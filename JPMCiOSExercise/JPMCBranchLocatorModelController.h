//
//  JPMCBranchLocatorModelController.h
//  JPMCiOSExercise
//
//  Created by Bedre, Karthik on 11/19/16.
//  Copyright © 2016 iOS Exercise. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JPMCGeoCoordinate.h"

@protocol JPMCBranchLocatorModelControllerDelegate <NSObject>

@required

/**
 @param locations : Carries the instances of JPMCBranchLocation class
 */
- (void)branchLocatorRequestCompletedWithResult:(NSArray *)locations;


/**
 @param error : Carries the error occurred during fetching the locations.
 */
- (void)branchLocatorRequestFailedWithError:(NSError *)error;

@end

@interface JPMCBranchLocatorModelController : NSObject

@property(nonatomic, weak) id<JPMCBranchLocatorModelControllerDelegate> delegate;


/**
	Use this method to fetch the locations around the coordinate's passed.

 @param coordinate : Pass the JPMCGeoCoordinate instance with lat and lng params.
 */
- (void)fetchLocationsForCoordinate:(JPMCGeoCoordinate *)coordinate;

@end
