//
//  JPMCBranchLocation.m
//  JPMCiOSExercise
//
//  Created by Bedre, Karthik on 11/18/16.
//  Copyright © 2016 iOS Exercise. All rights reserved.
//

#import "JPMCBranchLocation.h"
#import "JPMCUtility.h"

@implementation JPMCBranchLocation

- (CLLocationCoordinate2D)coordinate{
	
	return CLLocationCoordinate2DMake(self.lat.doubleValue, self.lng.doubleValue);
}

@end
