//
//  JPMCConstants.m
//  JPMCiOSExercise
//
//  Created by Bedre, Karthik on 11/18/16.
//  Copyright © 2016 iOS Exercise. All rights reserved.
//

#import "JPMCConstants.h"

NSString *const kClassJPMCBranchLocatorViewController = @"JPMCBranchLocatorViewController";
NSString *const kClassJPMCLocationDetailViewController = @"JPMCLocationDetailViewController";
