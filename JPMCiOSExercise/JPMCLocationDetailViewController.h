//
//  JPMCLocationDetailViewController.h
//  JPMCiOSExercise
//
//  Created by Bedre, Karthik on 11/19/16.
//  Copyright © 2016 iOS Exercise. All rights reserved.
//

#import "JPMCBaseViewController.h"
#import "JPMCBranchLocation.h"

@interface JPMCLocationDetailViewController : JPMCBaseViewController

@property(nonatomic, strong) id locationDetail;
@property(nonatomic, strong) NSString *navTitle;

@end
