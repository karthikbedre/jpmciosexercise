//
//  JPMCErrorManager.h
//  JPMCiOSExercise
//
//  Created by Bedre, Karthik on 11/19/16.
//  Copyright © 2016 iOS Exercise. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JPMCErrorManager : NSObject

+ (void)showAlertForError:(NSError *)error;

@end
