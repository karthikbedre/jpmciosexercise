//
//  JPMCUtility.m
//  JPMCiOSExercise
//
//  Created by Bedre, Karthik on 11/19/16.
//  Copyright © 2016 iOS Exercise. All rights reserved.
//

#import "JPMCUtility.h"

@implementation JPMCUtility

+ (void)showActivityIndicator:(BOOL)showIndicator{
	
	static NSInteger numberOfCallsToShow = 0;
	if (showIndicator)
		numberOfCallsToShow++;
	else
		numberOfCallsToShow--;
	
	NSAssert(numberOfCallsToShow >= 0, @"Network Activity Indicator was asked to hide more often than shown");
	
	// Display the indicator as long as our static counter is > 0.
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:(numberOfCallsToShow > 0)];
}

+ (NSString *)getSafeString:(NSString *)value{
	
	if(value && value.length > 0)
		return value;
	else
		return @"";
}

+ (void)showAlertWithtitle:(NSString *)title alertMessage:(NSString *)message delegateModel:(id)model tag:(int)tag cancelButtonTitle:(NSString *)cancelTitle otherTitles:(NSString *)titles, ...{
	
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
													message:message
												   delegate:model
										  cancelButtonTitle:cancelTitle
										  otherButtonTitles:nil];
	
	id eachObject;
	va_list argumentList;
	
	if (titles){
		// The first argument isn't part of the varargs list,
		// so we'll handle it separately.
		
		[alert addButtonWithTitle:titles];
		va_start(argumentList, titles); // Start scanning for arguments after firstObject.
		while ((eachObject = va_arg(argumentList, id)))  // As many times as we can get an argument of type "id"
			[alert addButtonWithTitle:eachObject]; // that isn't nil, add it to self's contents.
		va_end(argumentList);
	}
	alert.tag = tag;
	[alert show];
}

@end
