//
//  JPMCServiceEngine.m
//  JPMCiOSExercise
//
//  Created by Bedre, Karthik on 11/18/16.
//  Copyright © 2016 iOS Exercise. All rights reserved.
//

#import "JPMCServiceEngine.h"

@import AFNetworking;

// Declaring Endpoint for fetching locations, usually Host and ending Url are seperated to give flexibility for switching environment using application schemes by pre processor macros or Application environment.
static NSString *const kGetAtmBranchLocationAPI = @"https://m.chase.com/PSRWeb/location/list.action";

// Declare constants related to URLRequests
static NSString *const kContentTypeURLEncoded = @"application/x-www-form-urlencoded";
static NSString *const kContentType = @"Content-Type";


@implementation JPMCServiceEngine

+ (JPMCServiceEngine *)sharedInstance{
	
	static JPMCServiceEngine *singletonInstance = nil;
	static dispatch_once_t once;
	dispatch_once(&once, ^{
		singletonInstance = [[[self class] alloc] init];
	});
	
	return singletonInstance;
}

- (void)getAtmBranchLocationsForGeoCoordinate:(JPMCGeoCoordinate *)coordinate success:(serviceLocationsFetchComplete)successBlock failure:(serviceLocationsFetchfailed)failureBlock{
	
	AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
	manager.responseSerializer = [AFHTTPResponseSerializer serializer];
	manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringCacheData;
	[manager.requestSerializer setValue:kContentTypeURLEncoded forHTTPHeaderField:kContentType];

	[manager GET:kGetAtmBranchLocationAPI parameters:coordinate.mappedDictionary progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
		id data = [self serializeData:responseObject];
		if(data)
			successBlock(data);
		else
			failureBlock(nil);
	} failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
		failureBlock(error);
	}];
}

- (id)serializeData:(id)data{
	
	id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
	return result;
}

@end
