//
//  GMSMarker+JPMCGMSMarkerLocation.h
//  JPMCiOSExercise
//
//  Created by Bedre, Karthik on 11/19/16.
//  Copyright © 2016 iOS Exercise. All rights reserved.
//

#import <GoogleMaps/GoogleMaps.h>

@interface GMSMarker (JPMCGMSMarkerLocation)

@property(nonatomic, copy) id branchLocation;

@end
