//
//  JPMCServiceEngine.h
//  JPMCiOSExercise
//
//  Created by Bedre, Karthik on 11/18/16.
//  Copyright © 2016 iOS Exercise. All rights reserved.
//

/**
	Use this class to implement all Web Services related to application.
 */

#import <Foundation/Foundation.h>
#import "JPMCGeoCoordinate.h"

typedef void (^serviceLocationsFetchComplete)(id response);
typedef void (^serviceLocationsFetchfailed)(NSError *error);

@interface JPMCServiceEngine : NSObject

+ (JPMCServiceEngine *)sharedInstance;

/**
 Use this method to fetch the locations based on coordinates.

 @param coordinate  : Pass the JPMCGeoCoordinate instance.
 @param successBlock : Returns the Serialized JSON data.
 @param failureBlock : Returned if a error occured durning data fetch.
 */
- (void)getAtmBranchLocationsForGeoCoordinate:(JPMCGeoCoordinate *)coordinate success:(serviceLocationsFetchComplete)successBlock failure:(serviceLocationsFetchfailed)failureBlock;

@end
