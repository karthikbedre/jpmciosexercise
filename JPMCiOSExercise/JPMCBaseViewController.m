//
//  JPMCBaseViewController.m
//  JPMCiOSExercise
//
//  Created by Bedre, Karthik on 11/19/16.
//  Copyright © 2016 iOS Exercise. All rights reserved.
//

#import "JPMCBaseViewController.h"

@interface JPMCBaseViewController ()

@end

@implementation JPMCBaseViewController

- (id)init{
	
	self = [super initWithNibName:[self getNibName] bundle:nil];
	
	if (!self)
		self = [super init];
	
	return self;
}

- (void)viewDidLoad{
	
	[super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated{
	
	[super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated{
	
	[super viewDidAppear:animated];
	
	if(self.navigationTitle && !self.navigationItem.title)
		self.navigationItem.title = self.navigationTitle;
}

- (void)viewWillDisappear:(BOOL)animated{
	
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated{
	
	[super viewDidDisappear:animated];
}

- (void)setNavigationTitle:(NSString *)navigationTitle
{
	self.navigationItem.title = navigationTitle;
}

- (NSString *)getNibName{
	
	NSString *nibName = NSStringFromClass([self class]);
	NSString *fileExt = @"nib";
	
	if ([[NSBundle mainBundle] pathForResource:nibName ofType:fileExt] != nil){
		
		return nibName;
	}
	return @"";
}

@end
