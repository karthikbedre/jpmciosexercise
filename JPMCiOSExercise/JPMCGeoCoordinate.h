//
//  JPMCGeoCoordinate.h
//  JPMCiOSExercise
//
//  Created by Bedre, Karthik on 11/18/16.
//  Copyright © 2016 iOS Exercise. All rights reserved.
//

#import "JPMCBaseDataModel.h"

@interface JPMCGeoCoordinate : JPMCBaseDataModel

@property(nonatomic, strong) NSNumber *lat;
@property(nonatomic, strong) NSNumber *lng;

@end
