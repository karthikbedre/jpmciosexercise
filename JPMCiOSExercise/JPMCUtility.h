//
//  JPMCUtility.h
//  JPMCiOSExercise
//
//  Created by Bedre, Karthik on 11/19/16.
//  Copyright © 2016 iOS Exercise. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface JPMCUtility : NSObject

+ (void)showActivityIndicator:(BOOL)showIndicator;

+ (NSString *)getSafeString:(NSString *)value;

+ (void)showAlertWithtitle:(NSString *)title alertMessage:(NSString *)message delegateModel:(id)model tag:(int)tag cancelButtonTitle:(NSString *)cancelTitle otherTitles:(NSString *)titles, ...;

@end
