//
//  JPMCGoogleMapView.m
//  JPMCiOSExercise
//
//  Created by Bedre, Karthik on 11/18/16.
//  Copyright © 2016 iOS Exercise. All rights reserved.
//

#import "JPMCGoogleMapView.h"
#import "JPMCAppDelegate.h"
#import "JPMCConstants.h"
#import "JPMCScreenManager.h"
#import "GMSMarker+JPMCGMSMarkerLocation.h"

static NSString *const kGoogleAPIKey = @"AIzaSyB1DJQW0bucSPOo8MUB5pEXzv7BvIIbWsE";

static NSString *const kMapViewMyLocationKey = @"myLocation";

@interface JPMCGoogleMapView()<GMSMapViewDelegate>

@property (nonatomic, strong) GMSMapView *mapView;
@property (nonatomic) BOOL locationUpdated;

@end

@implementation JPMCGoogleMapView

- (void)awakeFromNib{
	
	[super awakeFromNib];
	[[self class] setAPIKey];
}

- (void)drawRect:(CGRect)rect{
	
	[self initiateMap];
}

+ (void)setAPIKey{
	
	[GMSServices provideAPIKey:kGoogleAPIKey];
}

- (void)initiateMap{
	
	if(!self.mapView){
		
		self.mapView = [[GMSMapView alloc]initWithFrame:self.frame];
		self.mapView.delegate = self;
		[self addSubview:self.mapView];
		
		[self.mapView addObserver:self
				   forKeyPath:kMapViewMyLocationKey
					  options:NSKeyValueObservingOptionNew
					  context:NULL];
		self.mapView.myLocationEnabled = YES;
	}
}

- (void)addLocationsInMapView:(NSArray *)locations{
	
	// Clear the avaialbe markers from Map.
	[self.mapView clear];
	
	// Create a Coordinates bound to make the map visible for all available locations.
	GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
	
	for(JPMCBranchLocation *location in locations){
		
		bounds = [bounds includingCoordinate:location.coordinate];
		GMSMarker *marker = [GMSMarker markerWithPosition:location.coordinate];
		marker.title = location.name;
		
		// branchLocation is a property of category GMSMarker+JPMCGMSMarkerLocation, use this to store the JPMCBranchLocation instance with marker
		marker.branchLocation = location;
		marker.map = self.mapView;
	}
	
	if(bounds.isValid)
		[self.mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds]];
}

#pragma mark - MapView Delegate
- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(nonnull GMSMarker *)marker{
	
	if([self.mapViewDelegate respondsToSelector:@selector(mapViewSelectedLocation:)] && marker.branchLocation)
		[self.mapViewDelegate mapViewSelectedLocation:marker.branchLocation];
	
	return YES;
}

#pragma Mark - KVO for kMapViewMyLocationKey listen for location changes.
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	
	if(!self.locationUpdated){
		
		self.locationUpdated = JPMCAppDelegate.isRechable;
		if(self.locationUpdated){
			
			CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
			self.mapView.camera = [GMSCameraPosition cameraWithTarget:location.coordinate
															 zoom:14];
			if([self.mapViewDelegate respondsToSelector:@selector(mapViewLoadedWithUserLocation)])
				[self.mapViewDelegate mapViewLoadedWithUserLocation];
		}
	}
}

- (JPMCGeoCoordinate *)currentCoordinate{
	
	JPMCGeoCoordinate *coordinate = [JPMCGeoCoordinate new];
	coordinate.lat = [NSNumber numberWithDouble:self.mapView.myLocation.coordinate.latitude];
	coordinate.lng = [NSNumber numberWithDouble:self.mapView.myLocation.coordinate.longitude];
	
	return coordinate;
}

- (void)dealloc{
	
	[self.mapView removeObserver:self forKeyPath:kMapViewMyLocationKey];
}
@end
