//
//  JPMCBranchLocatorViewController.m
//  JPMCiOSExercise
//
//  Created by Bedre, Karthik on 11/18/16.
//  Copyright © 2016 iOS Exercise. All rights reserved.
//

#import "JPMCBranchLocatorViewController.h"
#import "JPMCConstants.h"
#import "JPMCScreenManager.h"
#import "JPMCGoogleMapView.h"
#import "JPMCBranchLocatorModelController.h"
#import "JPMCLocationDetailViewController.h"
#import "JPMCErrorManager.h"

static NSString *const kNavigationTitle = @"ATM/Branch Locations";

@interface JPMCBranchLocatorViewController ()<JPMCMapViewDelegate, JPMCBranchLocatorModelControllerDelegate>

@property (nonatomic, weak) IBOutlet JPMCGoogleMapView *mapController;
@property (nonatomic, strong) JPMCBranchLocatorModelController *modelBranchLocator;

@end

@implementation JPMCBranchLocatorViewController

- (void)viewDidLoad {
	
    [super viewDidLoad];
	self.navigationTitle = kNavigationTitle;
}

#pragma mark - JPMCMapViewDelegate's

- (void)mapViewLoadedWithUserLocation{
	
	if(!self.modelBranchLocator)
		self.modelBranchLocator = [[JPMCBranchLocatorModelController alloc]init];
	self.modelBranchLocator.delegate = self;
	
	[self.modelBranchLocator fetchLocationsForCoordinate:self.mapController.currentCoordinate];
}

- (void)mapViewSelectedLocation:(JPMCBranchLocation *)location{
	
	JPMCLocationDetailViewController *controller = (JPMCLocationDetailViewController *)[[JPMCScreenManager sharedInstance] viewControllerWithName:kClassJPMCLocationDetailViewController];
	controller.locationDetail = location.mappedDictionary;
	
	[[JPMCScreenManager sharedInstance] navigateToViewController:controller animated:YES];
}

#pragma mark - JPMCBranchLocatorModelControllerDelegate

- (void)branchLocatorRequestCompletedWithResult:(NSArray *)locations{
	
	[self.mapController addLocationsInMapView:locations];
}

- (void)branchLocatorRequestFailedWithError:(NSError *)error{
	
	[JPMCErrorManager showAlertForError:error];
}

- (void)didReceiveMemoryWarning {
	
    [super didReceiveMemoryWarning];
}

@end
