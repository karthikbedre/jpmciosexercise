//
//  JPMCBranchLocatorModelController.m
//  JPMCiOSExercise
//
//  Created by Bedre, Karthik on 11/19/16.
//  Copyright © 2016 iOS Exercise. All rights reserved.
//

#import "JPMCBranchLocatorModelController.h"
#import "JPMCUtility.h"
#import "JPMCServiceEngine.h"
#import "JPMCBranchLocation.h"

static NSString *const kLocations = @"locations";

@implementation JPMCBranchLocatorModelController

- (void)fetchLocationsForCoordinate:(JPMCGeoCoordinate *)coordinate{
	
	[JPMCUtility showActivityIndicator:YES];
	
	[[JPMCServiceEngine sharedInstance] getAtmBranchLocationsForGeoCoordinate:coordinate success:^(id response) {
		
		[self filterResponseToDesiredObjects:response];
		
	} failure:^(NSError *error) {
		
		[self handleDelegateCallbackWithResult:nil error:error];
	}];
}

- (void)filterResponseToDesiredObjects:(id)response{
	
	NSMutableArray *arrayLocationCurated = [NSMutableArray new];
	if([response[kLocations]isKindOfClass:[NSArray class]] && [(NSArray *)response[kLocations] count] > 0){
		
		NSArray *arrayTraversel = response[kLocations];
		
		for(NSDictionary *dict in arrayTraversel){
			
			JPMCBranchLocation *location = [[JPMCBranchLocation alloc] init];
			NSDictionary *curatedDict = [location filterDictionaryForDesiredProperties:dict];
			
			BOOL mapSuccess = YES;
			@try {
				// Check if mapping to class objects succeeds.
				[location setValuesForKeysWithDictionary:curatedDict];
			} @catch (NSException *exception) {
				mapSuccess = NO;
			} @finally {
				if(mapSuccess)
					[arrayLocationCurated addObject:location];
			}
		}
	}
	
	[self handleDelegateCallbackWithResult:arrayLocationCurated error:nil];
}

- (void)handleDelegateCallbackWithResult:(NSArray *)locations error:(NSError *)error{
	
	[JPMCUtility showActivityIndicator:NO];
	if(error || !locations){
		if([self.delegate respondsToSelector:@selector(branchLocatorRequestFailedWithError:)])
			[self.delegate branchLocatorRequestFailedWithError:error];
		
	}
	else{
		if([self.delegate respondsToSelector:@selector(branchLocatorRequestCompletedWithResult:)])
			[self.delegate branchLocatorRequestCompletedWithResult:locations];
		
	}
}

@end
