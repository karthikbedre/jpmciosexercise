//
//  JPMCBaseViewController.h
//  JPMCiOSExercise
//
//  Created by Bedre, Karthik on 11/19/16.
//  Copyright © 2016 iOS Exercise. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JPMCBaseViewController : UIViewController

@property(nonatomic, strong) NSString *navigationTitle;

@end
