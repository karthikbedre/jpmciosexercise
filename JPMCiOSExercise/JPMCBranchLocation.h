//
//  JPMCBranchLocation.h
//  JPMCiOSExercise
//
//  Created by Bedre, Karthik on 11/18/16.
//  Copyright © 2016 iOS Exercise. All rights reserved.
//

#import "JPMCBaseDataModel.h"

@import CoreLocation;

@interface JPMCBranchLocation : JPMCBaseDataModel

@property(nonatomic, readonly) NSString *name;
@property(nonatomic, readonly) NSString *bank;
@property(nonatomic, readonly) NSNumber *atms;
@property(nonatomic, readonly) NSNumber *phone;
@property(nonatomic, readonly) NSArray *services;
@property(nonatomic, readonly) NSArray *lobbyHrs;
@property(nonatomic, readonly) NSArray *driveUpHrs;
@property(nonatomic, readonly) NSArray *languages;

@property(nonatomic, readonly) NSString *type;
@property(nonatomic, readonly) NSString *access;

@property(nonatomic, readonly) NSString *address;
@property(nonatomic, readonly) NSString *city;
@property(nonatomic, readonly) NSString *state;
@property(nonatomic, readonly) NSNumber *zip;

@property(nonatomic, readonly) NSString *locType;

@property(nonatomic, readonly) NSNumber *lat;
@property(nonatomic, readonly) NSNumber *lng;
@property(nonatomic, readonly) NSNumber *distance;

@property(nonatomic, readonly) CLLocationCoordinate2D coordinate;

@end
