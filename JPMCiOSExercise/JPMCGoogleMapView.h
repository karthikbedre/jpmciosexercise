//
//  JPMCGoogleMapView.h
//  JPMCiOSExercise
//
//  Created by Bedre, Karthik on 11/18/16.
//  Copyright © 2016 iOS Exercise. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JPMCGeoCoordinate.h"
#import "JPMCBranchLocation.h"

@import GoogleMaps;

@protocol JPMCMapViewDelegate <NSObject>

@optional

- (void)mapViewLoadedWithUserLocation;
- (void)mapViewSelectedLocation:(JPMCBranchLocation *)location;

@end

@interface JPMCGoogleMapView : UIView

@property(nonatomic, weak) IBOutlet id<JPMCMapViewDelegate> mapViewDelegate;
@property(nonatomic, readonly) JPMCGeoCoordinate *currentCoordinate;

/**
 Use this method to add GSMMarkers on Map View.
 
 @param locations : Pass array of objects JPMCBranchLocation.
 */
- (void)addLocationsInMapView:(NSArray *)locations;

@end
